'use strict';

angular.module('proyectoFullstackApp')
    .config(function ($stateProvider) {
        // $stateProvider
        //     .state('main.java', {
        //         url: 'java',
        //         templateUrl: 'app/main/java/java.html',
        //         controller: 'JavaCtrl',
        //         controllerAs: 'java'
        //     });
         $stateProvider
            .state('vista.java', {
                url: 'java',
                templateUrl: 'app/main/java/java.html',
                controller: 'JavaCtrl',
                controllerAs: 'java'
            });
    });