'use strict';

angular.module('proyectoFullstackApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('vista.angular', {
        url: 'angular',
        templateUrl: 'app/main/angular/angular.html',
        controller: 'AngularCtrl as angular'
      });
  });