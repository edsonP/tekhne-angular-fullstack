'use strict';

angular.module('proyectoFullstackApp')
    .config(function ($stateProvider) {
        // $stateProvider
        //     .state('main', {
        //         url: '/',
        //         templateUrl: 'app/main/main.html',
        //         controller: 'MainCtrl as main',
        //         redirectTo: 'main.java'
        //     });

            $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainCtrl as main',
                redirectTo: 'main'
            });
    });