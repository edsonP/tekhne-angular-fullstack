'use strict';

angular.module('proyectoFullstackApp')
    .controller('VistaCtrl', function ($scope,$http) {
        // Visual-Model
        var vm = this;
  

        $http.get('/api/productos').success(function (response) {
            vm.productos = response.productos;
        });

  

    });
