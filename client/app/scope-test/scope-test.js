'use strict';

angular.module('proyectoFullstackApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('scope-test', {
        url: '/scope-test',
        templateUrl: 'app/scope-test/scope-test.html'
//            ,
//        controller: 'ScopeTestCtrl'
      });
  });