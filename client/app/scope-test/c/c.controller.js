'use strict';

angular.module('proyectoFullstackApp')
    .controller('ControllerC', function ($scope) {
        var vm = this;
//        vm.propiedad = 'Valor C';
//        vm.propiedad = $scope.b.propiedad;
        vm.propiedad = $scope.$parent.a.propiedad;

        $scope.$on('mensaje-para-c',
            function(event, datos){
                console.log('[C] mensaje recibido', datos);
            });

        $scope.$emit('mensaje-para-a', {
            asunto: 'Saludos'
        });
    });
