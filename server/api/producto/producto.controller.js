/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/productos              ->  index
 */

'use strict';

// Gets a list of Productos
var jsonfile = require('jsonfile');
var path = require('path');
var FILE = path.resolve('db', 'productos.json');

// Get list of cursos
exports.getAll = function (req, res) {
    jsonfile.readFile(FILE, function (err, obj) {
        res.json(obj);
    });
};

exports.get = function (req, res) {
    jsonfile.readFile(FILE, function (err, obj) {
        res.json(obj.productos[req.params.id]);
    });
};