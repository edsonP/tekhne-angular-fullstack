'use strict';

var app = require('../..');
import request from 'supertest';

describe('Producto API:', function() {

  describe('GET /api/productos', function() {
    var productos;

    beforeEach(function(done) {
      request(app)
        .get('/api/productos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          productos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      productos.should.be.instanceOf(Array);
    });

  });

});
